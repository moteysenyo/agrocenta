<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Agrocenta</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../dist/css/style.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body >
    <div>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-2">
                    <div class="box">
                        <form role="form" id="dob_form" enctype="multipart/form-data">
                            <div class="box-body">

                                <div class="form-group">
                                    <label>Date</label>
                                    <div class="input-group">
                                        <input id="date" name="date" type="text" class="form-control" placeholder="YYYY-MM-DD" data-mask="____-__-__">
                                    </div>
                                </div>

                            </div>
                            <!-- /.box-body -->

                            <!-- /.box-body -->
                            <div class="box-footer">
                                <a href="../" class="btn btn-default pull-left">HOME</a>
                                <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-primary pull-right"/>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

    <!-- jQuery 2.2.3 -->
    <script src="../plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../plugins/fastclick/fastclick.js"></script>
    <!-- InputMask -->
    <script src="../plugins/input-mask/jquery.inputmask.js"></script>
    <script src="../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="../plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <script>
        Array.prototype.forEach.call(document.body.querySelectorAll("*[data-mask]"), applyDataMask);

        function applyDataMask(field) {
            var mask = field.dataset.mask.split('');
            
            // For now, this just strips everything that's not a number
            function stripMask(maskedData) {
                function isDigit(char) {
                    return /\d/.test(char);
                }
                return maskedData.split('').filter(isDigit);
            }
            
            // Replace `_` characters with characters from `data`
            function applyMask(data) {
                return mask.map(function(char) {
                    if (char != '_') return char;
                    if (data.length == 0) return char;
                    return data.shift();
                }).join('')
            }
            
            function reapplyMask(data) {
                return applyMask(stripMask(data));
            }
            
            function changed() {   
                var oldStart = field.selectionStart;
                var oldEnd = field.selectionEnd;
                
                field.value = reapplyMask(field.value);
                
                field.selectionStart = oldStart;
                field.selectionEnd = oldEnd;
            }
            
            field.addEventListener('click', changed)
            field.addEventListener('keyup', changed)
        }

        $(document).ready(function(e){
            // Submit form data via Ajax
            $("#dob_form").on('submit', function(e) {
                e.preventDefault();
                $.ajax({
                    type: 'post',
                    url: '../scripts/parse_date.php',
                    data: new FormData(this),
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData:false,
                    beforeSend: function(){

                    },
                    success: function(response){ 
                        //console.log(response);

                        alert(response.message);
                    },
                    error: function(xhr, status, error){ 
                        //console.log(response);
                        //alert(xhr.responseText);
                    }
                });
            });
        });
    </script>
</body>

</html>