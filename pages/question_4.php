<?php
    require_once '../api/config/db.php';
    require_once '../api/objects/object.php';
    
    $database = new Database();
    $db = $database->getConnection();
      
    // initialize object
    $object = new Api($db);

    $users = $object->users();
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Agrocenta</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../dist/css/style.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body >
    <div>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Users</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table id="example1" class="table table-bordered table-striped fixed">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Gender</th>
                                        <th>Town</th>
                                        <th>GPS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        if ($users['success']) {
                                            $rows = $users['data'];

                                            foreach ($rows as $row) {
                                                echo '
                                                <tr data-row-id="'.$row['id'].'">
                                                    <td>'.$row['id'].'</td>
                                                    <td><a href="question_4_detail.php?id='.$row['id'].'">'.$row['name'].'</a></td>
                                                    <td>'.$row['gender'].'</td>
                                                    <td>'.$row['town'].'</td>
                                                    <td>'.$row['gps'].'</td>
                                                </tr>';
                                            }
                                        }
                                    ?>
                                </tbody>
                            </table>

                            <!-- /.box-body -->
                        <div class="box-footer">
                            <a href="../"><span class="btn btn-default pull-left">Home</span></a>
                        </div>
                        <!-- /.box-footer -->
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>

    <!-- jQuery 2.2.3 -->
    <script src="../plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../plugins/fastclick/fastclick.js"></script>
    <script>
        $(function() {
            $("#example1").DataTable({
                "paging": false,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": false,
                "autoWidth": false
            });
            $('#example2').DataTable({
                
            });
        });
    </script>
</body>

</html>