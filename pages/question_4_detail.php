<?php
    require_once '../api/config/db.php';
    require_once '../api/objects/object.php';
    
    $database = new Database();
    $db = $database->getConnection();
      
    // initialize object
    $object = new Api($db);

    $user = $object->user();
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Agrocenta</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../dist/css/style.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>

<body >
    <div>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">

                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">User Detail</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label>ID</label>
                                <div class="input-group">
                                    <input id="id" name="id" class="form-control" disabled>
                                </div>
                                <!-- /.input group -->
                            </div>
                            <!-- /.form group -->

                            <div class="form-group">
                                <label>Name</label>
                                <div class="input-group">
                                    <input id="name" name="name" class="form-control" disabled>
                                </div>
                                <!-- /.input group -->
                            </div>
                            <!-- /.form group -->

                            <div class="form-group">
                                <label>Gender</label>
                                <div class="input-group">
                                    <input id="gender" name="gender" class="form-control" disabled>
                                </div>
                                <!-- /.input group -->
                            </div>
                            <!-- /.form group -->

                            <div class="form-group">
                                <label>Town</label>
                                <div class="input-group">
                                    <input id="town" name="town" class="form-control" disabled>
                                </div>
                                <!-- /.input group -->
                            </div>
                            <!-- /.form group -->

                            <div class="form-group">
                                <label>GPS</label>
                                <div class="input-group">
                                    <input id="gps" name="gps" class="form-control" disabled>
                                </div>
                                <!-- /.input group -->
                            </div>
                            <!-- /.form group -->

                        </div>
                        <!-- /.box-body -->

                        <!-- /.box-body -->
                        <div class="box-footer">
                            <a href="question_4.php"><span class="btn btn-default pull-left">List</span></a>
                            <button class="btn btn-default pull-right" onclick="next()"><i class="fa fa-arrow-right"></i></button>
                            <button class="btn btn-default pull-right" onclick="previous()"><i class="fa fa-arrow-left"></i></button>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                    <!-- /.box -->

                </div>

            </div>
            <!-- /.row -->

        </section>
        <!-- /.content -->
    </div>

    <!-- jQuery 2.2.3 -->
    <script src="../plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/app.min.js"></script>
    <!-- DataTables -->
    <script src="../plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.12.3.js"></script>
    <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function(){
            user();
        });

        function user() {
            var id = "<?php echo $_GET['id']; ?>";

            $.ajax({
                type: "post",
                url: '../scripts/user.php?id='+id,
                dataType: 'json',
                data: {id:id},
                cache: false,
                contentType: false,
                processData:false,
                beforeSend: function(){
                    
                },
                success: function(response){ 
                    console.log(response);

                    if (response.success) {
                        // successful
                        document.getElementById("id").value = response.data['id'];
                        document.getElementById("name").value = response.data['name'];
                        document.getElementById("gender").value = response.data['gender'];
                        document.getElementById("town").value = response.data['town'];
                        document.getElementById("gps").value = response.data['gps'];
                        document.getElementById("gender").value = response.data['gender'];
                    } else if (!response.success) {
                        // unsuccessful;
                    }
                },
                error: function(xhr, status, error){ 
                    console.log(response);
                    //alert(xhr.responseText);
                }
            });
        }

        function previous() {
            var id = $('#id').val();

            $.ajax({
            type: 'post',
                url: '../scripts/previous.php?id='+id,
                dataType: 'json',
                data:{id:id},
                cache: false,
                contentType: false,
                processData:false,
                beforeSend: function(){

                },
                success: function(response){ 
                    console.log(response);

                    if (response.success) {
                        // successful
                        document.getElementById("id").value = response.data['id'];
                        document.getElementById("name").value = response.data['name'];
                        document.getElementById("gender").value = response.data['gender'];
                        document.getElementById("town").value = response.data['town'];
                        document.getElementById("gps").value = response.data['gps'];
                        document.getElementById("gender").value = response.data['gender'];
                    } else if (!response.success) {
                        alert(response.message);
                    }
                },
                error: function(xhr, status, error){ 
                    //console.log(response);
                    //alert(xhr.responseText);
                }
            });
        }

        function next() {
            var id = $('#id').val();

            $.ajax({
            type: 'post',
                url: '../scripts/next.php?id='+id,
                dataType: 'json',
                data:{id:id},
                cache: false,
                contentType: false,
                processData:false,
                beforeSend: function(){

                },
                success: function(response){ 
                    console.log(response);

                    if (response.success) {
                        // successful
                        document.getElementById("id").value = response.data['id'];
                        document.getElementById("name").value = response.data['name'];
                        document.getElementById("gender").value = response.data['gender'];
                        document.getElementById("town").value = response.data['town'];
                        document.getElementById("gps").value = response.data['gps'];
                        document.getElementById("gender").value = response.data['gender'];
                    } else if (!response.success) {
                        alert(response.message);
                    }
                },
                error: function(xhr, status, error){ 
                    //console.log(response);
                    //alert(xhr.responseText);
                }
            });
        }
    </script>
</body>

</html>