<?php
extract($_POST);

if (isset($_POST["date"]) && 
	!empty($_POST["date"])) {

	$date = DateTime::createFromFormat('Y-m-d', $_POST["date"]);

    $validity = $date && $date->format('Y-m-d') === $_POST["date"];

    if ($validity) {
        $message = "the date you entered is valid";
    } else {
        $message = "the date you entered is invalid. please enter the date in the format YYY-MM-DD";
    }

    $response = array("success" => $validity,
	                "message" => $message);

    echo json_encode($response);
} else {
	$response = array("success" => false,
	                "message" => "please complete the form");

    echo json_encode($response);
}
?>