<?php
require_once '../api/config/db.php';
require_once '../api/objects/object.php';

$database = new Database();
$db = $database->getConnection();
  
// initialize object
$object = new Api($db);

if (isset($_GET["id"]) && !empty($_GET["id"])) {
	$object->id  = $_GET["id"];

    $response = $object->previous();

    echo json_encode($response);

} else {
	$response = array("success" => false,
	                "message" => "no ID received by the server");

    echo json_encode($response);
}