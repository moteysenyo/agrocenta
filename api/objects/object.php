<?php
class Api {

    // database connection and table name
    private $conn;
    private $table_users = "users";

    public $id;

    // constructor with $db as database connection
    public function __construct($db) {
        $this->conn = $db;
    }

    function users() {
        $query = "SELECT
                    id, 
                    name, 
                    gender,
                    town,
                    gps 
                FROM 
                    ".$this->table_users;

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // execute query
        if ($stmt->execute()) {
            if ($stmt->rowCount() > 0) {
                $data = array();

                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    array_push($data, $row);
                }

                $result = array("success" => true,
                                "message" => "users found",
                                "data" => $data);

                return $result;
            } elseif ($stmt->rowCount() == 0) {
                $result = array("success" => false,
                                "message" => "no users found");

                return $result;
            }
        } else {
            $result = array("success" => false,
                            "message" => "something went wrong");

            return $result;
        }
    }

    function user() {
        $this->id = htmlspecialchars(strip_tags($this->id));
        
        $query = "SELECT
                    id, 
                    name, 
                    gender,
                    town,
                    gps 
                FROM 
                    ".$this->table_users."
                WHERE 
                    id='".$this->id."'";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // execute query
        if ($stmt->execute()) {
            if ($stmt->rowCount() > 0) {
                $row = $stmt->fetch(PDO::FETCH_ASSOC);

                $data = array("id" => $row["id"],
                            "name" => $row["name"],
                            "gender" => $row["gender"],
                            "town" => $row["town"],
                            "gps" => $row["gps"]);

                $result = array("success" => true,
                                "message" => "user found",
                                "data" => $data);

                return $result;
            } elseif ($stmt->rowCount() == 0) {
                $result = array("success" => false,
                                "message" => "no user found");

                return $result;
            }
        } else {
            $result = array("success" => false,
                            "message" => "something went wrong");

            return $result;
        }
    }

    function previous() {
        $this->id = htmlspecialchars(strip_tags($this->id));

        $users = $this->users();
        if ($users['success']) {
            for($i=0; $i<count($users['data']); $i++) {
                if ($users['data'][0]['id'] == $this->id) {
                    $result = array("success" => false,
                                    "message" => "this is the first user on the list");

                    return $result;
                }

                if ($users['data'][$i]['id'] == $this->id) {
                    $ii = $i - 1;
                    $this->id = $users['data'][$ii]['id'];

                    break;
                }
            }
        }

        $query = "SELECT
                    id,
                    name, 
                    gender,
                    town,
                    gps 
                FROM 
                    ".$this->table_users."
                WHERE 
                    id='".$this->id."'";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // execute query
        if ($stmt->execute()) {
            if ($stmt->rowCount() > 0) {
                $row = $stmt->fetch(PDO::FETCH_ASSOC);

                $data = array("id" => $row["id"],
                            "name" => $row["name"],
                            "gender" => $row["gender"],
                            "town" => $row["town"],
                            "gps" => $row["gps"]);

                $result = array("success" => true,
                                "message" => "user found",
                                "data" => $data);

                return $result;
            } elseif ($stmt->rowCount() == 0) {
                $result = array("success" => false,
                                "message" => "no user found");

                return $result;
            }
        } else {
            $result = array("success" => false,
                            "message" => "something went wrong");

            return $result;
        }
    }

    function next() {
        $this->id = htmlspecialchars(strip_tags($this->id));

        $users = $this->users();
        if ($users['success']) {
            for($i=0; $i<count($users['data']); $i++) {
                $j = count($users['data']) - 1;
                if ($users['data'][$j]['id'] == $this->id) {
                    $result = array("success" => false,
                                    "message" => "this is the last user on the list");

                    return $result;
                }

                if ($users['data'][$i]['id'] == $this->id) {
                    $ii = $i + 1;
                    $this->id = $users['data'][$ii]['id'];

                    break;
                }
            }
        }

        $query = "SELECT
                    id,
                    name, 
                    gender,
                    town,
                    gps 
                FROM 
                    ".$this->table_users."
                WHERE 
                    id='".$this->id."'";

        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // execute query
        if ($stmt->execute()) {
            if ($stmt->rowCount() > 0) {
                $row = $stmt->fetch(PDO::FETCH_ASSOC);

                $data = array("id" => $row["id"],
                            "name" => $row["name"],
                            "gender" => $row["gender"],
                            "town" => $row["town"],
                            "gps" => $row["gps"]);

                $result = array("success" => true,
                                "message" => "user found",
                                "data" => $data);

                return $result;
            } elseif ($stmt->rowCount() == 0) {
                $result = array("success" => false,
                                "message" => "no user found");

                return $result;
            }
        } else {
            $result = array("success" => false,
                            "message" => "something went wrong");

            return $result;
        }
    }
}

?>